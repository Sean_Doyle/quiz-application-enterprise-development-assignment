var modal = document.getElementById("modal-innerWrapper");
var modalDefault = modal.innerHTML;

if(Parse.User.current()){
	document.getElementById("login").style.display = 'none';
	document.getElementById("options").style.display = 'block';

	var cu = document.getElementById("currentUser");
	cu.innerHTML = "<b>Current User: </b>" + Parse.User.current().attributes.username + " <button onclick='logout()'>Logout</button><hr>";
	console.log(Parse.User.current());
	cu.style.display = 'block';
}

function login(){
	location.href="#openModal";

	modal.innerHTML = modalDefault;

	var header = document.createElement("h2");
	header.innerHTML = "Login <hr>";
	modal.appendChild(header);

	var userInfo = document.createElement("p");
	userInfo.innerHTML = "Username: <br />";

	var userName = document.createElement("input");
	userName.setAttribute("type","text");
	userName.setAttribute("id","loginUser-un");
	userInfo.appendChild(userName);

	userInfo.innerHTML = userInfo.innerHTML + "<br /> Password: <br />";
	var password = document.createElement("input");
	password.setAttribute("type","password");
	password.setAttribute("id","loginUser-pw");
	userInfo.appendChild(password);

	var submit = document.createElement("button");
	submit.setAttribute("onclick","loginUser()");
	submit.innerHTML = "Submit";

	modal.appendChild(userInfo);
	modal.appendChild(submit);
}

function signUp(){
	location.href="#openModal";

	modal.innerHTML = modalDefault;

	var header = document.createElement("h2");
	header.innerHTML = "Sign Up To Quiz Central <hr>";
	modal.appendChild(header);

	var userInfo = document.createElement("p");
	userInfo.innerHTML = "Enter a username you wish to use: <br />";

	var userName = document.createElement("input");
	userName.setAttribute("type","text");
	userName.setAttribute("id","createUser-un");
	userInfo.appendChild(userName);

	userInfo.innerHTML = userInfo.innerHTML + "<br /> Enter a password: <br />";
	var password = document.createElement("input");
	password.setAttribute("id","createUser-pw");
	password.setAttribute("type","password");
	userInfo.appendChild(password);

	userInfo.innerHTML = userInfo.innerHTML + "<br /> Enter your email: <br />";
	var email = document.createElement("input");
	email.setAttribute("id","createUser-email");
	email.setAttribute("type","text");
	userInfo.appendChild(email);

	var submit = document.createElement("button");
	submit.setAttribute("onclick","createUser()");
	submit.innerHTML = "Submit";

	modal.appendChild(userInfo);
	modal.appendChild(submit);
}

function loginUser(){
	var un = document.getElementById("loginUser-un").value;
	var pw = document.getElementById("loginUser-pw").value;

	Parse.User.logIn(un,pw, {
		success: function(){
			document.getElementById("login").style.display = 'none';
			document.getElementById("options").style.display = 'block';

			var cu = document.getElementById("currentUser");
			cu.innerHTML = "<b>Current User: </b>" + un + " <button onclick='logout()'>Logout</button><hr>";
			cu.style.display = 'block';

			location.href="#close";
		},
		error: function(user, error){
			alert("Error: " + error.code + " " + error.message);
		}
	})
}

function createUser(){
	var un = document.getElementById("createUser-un").value;
	var pw = document.getElementById("createUser-pw").value;
	var email = document.getElementById("createUser-email").value;

	var user = new Parse.User();
	user.set("username",un);
	user.set("password",pw);
	user.set("email",email);

	user.signUp(null, {
		success: function(){
			document.getElementById("login").style.display = 'none';
			document.getElementById("options").style.display = 'block';

			var cu = document.getElementById("currentUser");
			cu.innerHTML = "<b>Current User: </b>" + un + " <button onclick='logout()'>Logout</button><hr>";
			cu.style.display = 'block';

			location.href="#close";
		},
		error: function(user, error){
			alert("Error: " + error.code + " " + error.message);
		}
	})
}

function logout(){
	Parse.User.logOut();

	document.getElementById("login").style.display = 'block';
	document.getElementById("options").style.display = 'none';
	document.getElementById("currentUser").style.display = 'none';
}
