getQuestions();

function getQuestions() {
	var Question = Parse.Object.extend("QuizQuestion");
	var qst = new Parse.Query(Question);
	qst.find({
		success: function (data) {
			createQstTable(data);
		},
		error: function () {
			console.log("Parse query failed when retrieving question list.");
		}
	});
}

function createQstTable(data){
	var table = document.getElementById("questionTable");
	table.setAttribute("style","width:100%");
	table.innerHTML = "<td><b>Title:</b></td><td><b>Details:</b></td><td><b>Answer:</b></td>"

	for(var index in data){
		var tableRow = document.createElement("tr");

		var dataTitle = document.createElement("td");
		dataTitle.innerHTML = data[index].attributes.title;
		tableRow.appendChild(dataTitle);

		var dataQuestion = document.createElement("td");
		dataQuestion.innerHTML = data[index].attributes.question;
		tableRow.appendChild(dataQuestion);

		var qType = data[index].attributes.type;
		if(qType.match("qa") || qType.match("tf")){
			var dataAnswer = document.createElement("td");
			dataAnswer.innerHTML = data[index].attributes.answer;
			tableRow.appendChild(dataAnswer);

		}else if(qType.match("mc")){
			var dataAnswer = document.createElement("td");
			dataAnswer.innerHTML = data[index].attributes.answer + ", " + data[index].attributes.fakeOne + ", " + data[index].attributes.fakeTwo + ", " + data[index].attributes.fakeThree;
			tableRow.appendChild(dataAnswer);
		}

		var qstDelete = document.createElement("td");

		var href = document.createElement("a");
		href.setAttribute("id","" + data[index].id);
		href.setAttribute("href","#");
		href.setAttribute("onclick","deleteQuestion(this.id)");
		href.innerHTML = "Delete";

		qstDelete.appendChild(href);
		tableRow.appendChild(qstDelete);

		table.appendChild(tableRow);
	}
}

function deleteQuestion(id){
	var Question = Parse.Object.extend("QuizQuestion");
	var qst = new Parse.Query(Question);
	qst.get(id, {
		success: function(myQuestion){
			myQuestion.destroy({
				success: function(myQuestion){
					checkQuizzes(myQuestion);
					location.reload();
				},
				error: function(myQuestion, error){
					console.log(error);
				}
			});
		}
	})
}

function checkQuizzes(qst) {
	//Get Quizzes
	var Quiz = Parse.Object.extend("Quiz");
	var quiz = new Parse.Query(Quiz);
	quiz.find({
		success: function (data) {
			//Check if quiz has this question
			for (var index in data) {
				if (data[index].id.match(qst.id)) {
					editQuiz(data[index], qst);
				}
			}
		},
		error: function () {
			console.log("Parse query failed when retrieving question list.");
		}
	});
}

function editQuiz(exam, qst){
	var tempQuiz = exam;
	tempQuiz.attributes.questions.splice(tempQuiz.attributes.questions.indexOf(qst.attributes.title));
	tempQuiz.attributes.questionIDs.splice(tempQuiz.attributes.questionIDs.indexOf(qst.id));

	var Quiz = Parse.Object.extend("Quiz");
	var quiz = new Parse.Query(Quiz);
	quiz.save(null, {
		success: function(myQuiz){
			myQuiz.set("questions", tempQuiz.attributes.questions);
			myQuiz.set("questionIDs", tempQuiz.attributes.questionIDs);
			myQuiz.save();
		}
	});
}