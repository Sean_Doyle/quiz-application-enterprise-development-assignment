var questions;
var quizQuestions = [];
var quizQuestionIDs = [];
var bodyStorage;

getQuestions();
function getQuestions() {
	var Question = Parse.Object.extend("QuizQuestion");
	var qst = new Parse.Query(Question);
	qst.find({
		success: function (data) {
			questions = data;
			createQstTable(data);
		},
		error: function () {
			console.log("Parse query failed when retrieving question list.");
		}
	});
}

function createQstTable(data){
	var table = document.getElementById("questionListingTable");
	table.setAttribute("style","width:100%");
	table.innerHTML = "<td><b>Title:</b></td><td><b>Details:</b></td>";
	for(var index in data){
		var tableRow = document.createElement("tr");

		var tableDataTitle = document.createElement("td");
		tableDataTitle.innerHTML = data[index].attributes.title;
		tableRow.appendChild(tableDataTitle);

		var tableDataID = document.createElement("td");
		tableDataID.innerHTML = data[index].attributes.question;
		tableRow.appendChild(tableDataID);

		var addQst = document.createElement("td");

		var href = document.createElement("a");
		href.setAttribute("id","" + data[index].id);
		href.setAttribute("href","#");
		href.setAttribute("onclick","addQuestionToQuiz(this.id)");
		href.innerHTML = "Add";

		addQst.appendChild(href);
		tableRow.appendChild(addQst);

		table.appendChild(tableRow);
	}
	document.getElementById("questionListing").appendChild(table);

	var bodyTag = document.getElementsByTagName("body")[0];
	bodyStorage = bodyTag.innerHTML;
}

function updateQuizForm(){
	var table = document.getElementById("quizFormTable");
	table.innerHTML = "<td><b>Title:</b></td><td><b>Details:</b></td>";
	table.setAttribute("style","width:100%");

	for(var index in quizQuestions){
		var tableRow = document.createElement("tr");

		var tableDataTitle = document.createElement("td");
		tableDataTitle.innerHTML = quizQuestions[index].attributes.title;
		tableRow.appendChild(tableDataTitle);

		var tableDataID = document.createElement("td");
		tableDataID.innerHTML = quizQuestions[index].attributes.question;
		tableRow.appendChild(tableDataID);

		table.appendChild(tableRow);
	}
}

function addQuestionToQuiz(id){
	var href = document.getElementById(id);
	href.setAttribute("onclick","removeQuestionFromQuiz(this.id)");
	href.innerHTML = "Remove";

	if(searchQuiz(id) == -1) {
		for (var index in questions) {
			if (id.match(questions[index].id)) {
				quizQuestions.push(questions[index]);
			}
		}
	}

	updateQuizForm();
}

function removeQuestionFromQuiz(id){
	var href = document.getElementById(id);
	href.setAttribute("onclick","addQuestionToQuiz(this.id)");
	href.innerHTML = "Add";

	if(searchQuiz(id) != -1) {
		quizQuestions.splice(searchQuiz(id),1);
	}

	updateQuizForm()
}

function searchQuiz(id){
	for(var index in quizQuestions){
		if(id.match(quizQuestions[index].id))return index;
	}
	return -1;
}

function randomQuiz(){
	var quizNotDone = true;
	var questionNumbers = [];
	while(quizNotDone){
		var num = Math.floor(Math.random() * questions.length);
		if(questionNumbers.indexOf(num) == -1){
			quizQuestions.push(questions[num]);
			questionNumbers.push(num);
		}
		if(quizQuestions.length == 10 || quizQuestions.length == questions.length){ quizNotDone = false; }
	}

	saveQuiz();
}

function saveQuiz() {
	var quizToSave = {};
	var qstIDs = [];
	var qstMarks = [];
	var qsts = [];

	var quizMarks = document.getElementById("quizMarks").value;
	var marks = quizMarks%quizQuestions.length;

	for(var index in quizQuestions){
		//qstMarks do not divide in evenly
		if(index == 0 && marks != 0){
			qstMarks.push(Math.floor(quizMarks/quizQuestions.length) + marks);
		} else{
			qstMarks.push(Math.floor(quizMarks/quizQuestions.length));
		}

		qsts.push(quizQuestions[index].attributes.title);
		qstIDs.push(quizQuestions[index].id);
	}

	quizToSave.QuizName = document.getElementById("quizNameEntry").value;
	quizToSave.Description = document.getElementById("quizDescription").value;
	quizToSave.questions = qsts;
	quizToSave.marks = qstMarks;
	quizToSave.questionIDs = qstIDs;

	console.log(quizQuestions);
	var Quiz = Parse.Object.extend("Quiz");
	var quiz = new Quiz();
	quiz.save(quizToSave).then(function (object) {
		quizQuestions = [];
		quizQuestionIDs = [];
	});

	var bodyTag = document.getElementsByTagName("body")[0];
	bodyTag.innerHTML = bodyStorage;
}