
//Question Head
var wrapperDiv;
var pg_divider_top;
var qstName, qstToAsk;

//Question Footer
var pg_divider_bottom;
var submit;

//Question Body - Multiple Choice
var ansOne, ansTwo,ansThree, ansFour;

//Question Body - True False
var divTglBtn;
var lblTrue, lblFalse;

//Question Body - Qst & Ans
var ans;

function setup(){
	//Clear Div
	document.getElementById("questionType").innerHTML = "";

	//Create Head
	wrapperDiv = document.getElementById("questionType");
	pg_divider_top = document.createElement("hr");

	qstName = document.createElement("input");
	qstName.setAttribute("type","text");
	qstName.setAttribute("id","qstName");
	qstName.setAttribute("placeholder","Question Name:");

	qstToAsk = document.createElement("textarea");
	qstToAsk.setAttribute("id","qstToAsk");
	qstToAsk.setAttribute("rows","8");
	qstToAsk.setAttribute("cols","50");
	qstToAsk.setAttribute("placeholder","What Question do you wish to pose?");

	//Append Head
	wrapperDiv.appendChild(pg_divider_top);
	wrapperDiv.appendChild(qstName);
	insertLineBreak();
	wrapperDiv.appendChild(qstToAsk);
	insertLineBreak();

	//Get Body Type
	var type = document.getElementById("qstType").value;

	//Create Body
	if(type.match("mc")){ createMultipleChoice(); }
	else if(type.match("tf")){ createTrueFalse(); }
	else if(type.match("qa")){ createQstAns(); }

	//Create Footer
	pg_divider_bottom = document.createElement("hr");

	submit = document.createElement("button");
	submit.setAttribute("id","submit");
	submit.setAttribute("onclick","validateForm()");
	submit.innerHTML = "Submit";

	//Append Footer
	wrapperDiv.appendChild(pg_divider_bottom);
	wrapperDiv.appendChild(submit);

	if(type.match("default")){
		while (wrapperDiv.lastChild) {
		wrapperDiv.removeChild(wrapperDiv.lastChild);
	}}
}

function createMultipleChoice(){
	ansOne = document.createElement("input");
	ansOne.setAttribute("type","text");
	ansOne.setAttribute("id","ansOne");
	ansOne.setAttribute("placeholder","Correct Answer:");

	ansTwo = document.createElement("input");
	ansTwo.setAttribute("type","text");
	ansTwo.setAttribute("id","ansTwo");
	ansTwo.setAttribute("placeholder","Second Answer:");

	ansThree = document.createElement("input");
	ansThree.setAttribute("type","text");
	ansThree.setAttribute("id","ansThree");
	ansThree.setAttribute("placeholder","Third Answer:");

	ansFour = document.createElement("input");
	ansFour.setAttribute("type","text");
	ansFour.setAttribute("id","ansFour");
	ansFour.setAttribute("placeholder","Fourth Answer:");

	//Append Body
	wrapperDiv.appendChild(ansOne);
	insertLineBreak();
	wrapperDiv.appendChild(ansTwo);
	insertLineBreak();
	wrapperDiv.appendChild(ansThree);
	insertLineBreak();
	wrapperDiv.appendChild(ansFour);
	insertLineBreak();
}

function createTrueFalse(){
	divTglBtn = document.createElement("div");
	divTglBtn.setAttribute("class","toggle-btn-grp");

	lblTrue = document.createElement("label");
	lblTrue.setAttribute("class","toggle-btn success");
	lblTrue.innerHTML = '<input type="radio" name="trueFalse" class="visuallyhidden" value="true" checked/>True';

	lblFalse = document.createElement("label");
	lblFalse.setAttribute("class","toggle-btn");
	lblFalse.innerHTML= '<input type="radio" name="trueFalse" class="visuallyhidden" value="false" />False';

	//Append Body
	wrapperDiv.appendChild(divTglBtn);
	divTglBtn.appendChild(lblTrue);
	divTglBtn.appendChild(lblFalse);
}

function createQstAns() {
	ans = document.createElement("input");
	ans.setAttribute("type","text");
	ans.setAttribute("id","ans");
	ans.setAttribute("placeholder","Answer: ");

	//Append Body
	wrapperDiv.appendChild(ans);
	insertLineBreak();
}

function insertLineBreak(){
	wrapperDiv.appendChild(document.createElement("br"));
}

function validateForm(){
	var question = {};
	var error = {};

	var qstType = document.getElementById("qstType").value;
	var qstTitle = document.getElementById("qstName").value;
	var qstAsked = document.getElementById("qstToAsk").value;

	question.type = qstType;

	if(qstTitle != "") question.title = qstTitle; else error.title = "Title cannot be blank.";
	if(qstAsked != "") question.question = qstAsked; else error.asked = "Question cannot be blank.";

	if(qstType.match("mc")){
		var ans_One = document.getElementById("ansOne").value;
		var ans_Two = document.getElementById("ansTwo").value;
		var ans_Three = document.getElementById("ansThree").value;
		var ans_Four = document.getElementById("ansFour").value;

		if(ans_One != "") question.answer = ans_One; else error.correct = "Please provide an answer.";
		if(ans_Two != "") question.fakeOne = ans_Two; else error.fa1 = "Please provide the first fake answer.";
		if(ans_Three != "") question.fakeTwo = ans_Three; else error.fa2 = "Please provide the second fake answer.";
		if(ans_Four != "") question.fakeThree = ans_Four; else error.fa3 = "Please provide the third fake answer.";

	} else if(qstType.match("tf")){
		//No validation required as it defaults to True;
		question.answer = document.querySelector('input[name="trueFalse"]:checked').value;

	} else if(qstType.match("qa")){
		var qst_Ans = document.getElementById("ans").value;
		if(qst_Ans != "") question.answer = qst_Ans; else error.qa = "Please provide the answer to the question.";

	}

	if(jQuery.isEmptyObject(error)) {
		var Question = Parse.Object.extend("QuizQuestion");
		var qst = new Question();
		qst.save(question).then(function (object) {
			console.log("Saved to parse successfully:" + JSON.stringify(object));
			clearForm();
		});
	} else {
		console.log(JSON.stringify(error));
	}
}

function clearForm(){
	var qstType = document.getElementById("qstType").value;

	document.getElementById("qstName").value = "";
	document.getElementById("qstToAsk").value = "";
	if(qstType.match("mc")){
		document.getElementById("ansOne").value = "";
		document.getElementById("ansTwo").value = "";
		document.getElementById("ansThree").value = "";
		document.getElementById("ansFour").value = "";
	} else if(qstType.match("qa")) {
		document.getElementById("ans").value = "";
	}
}

$(".toggle-btn input[type=radio]").addClass("visuallyhidden");
$("body").on("change", ".toggle-btn input[type=radio]", function() {
	if( $(this).attr("name") ) {
		$(this).parent().addClass("success").siblings().removeClass("success")
	} else {
		$(this).parent().toggleClass("success");
	}
});