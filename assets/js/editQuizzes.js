getQuizzes();
getQuestions();

var questions = [];
var quizzes = [];

var bodyStorage;

function getQuizzes() {
	var Quiz = Parse.Object.extend("Quiz");
	var quiz = new Parse.Query(Quiz);
	quiz.find({
		success: function (data) {
			quizzes = data;
			createQuizTable(quizzes);
		},
		error: function () {
			console.log("Parse query failed when retrieving question list.");
		}
	});
}

function getQuestions(){
	var Question = Parse.Object.extend("QuizQuestion");
	var qst = new Parse.Query(Question);
	qst.find({
		success: function (data) {
			questions = data;
		},
		error: function () {
			console.log("Parse query failed when retrieving question list.");
		}
	});
}

function createQuizTable(data){
	var table = document.getElementById("quizTable");
	table.setAttribute("style","width:100%");
	table.innerHTML = "<td><b>Title:</b></td><td><b>Description:</b></td>"

	for(var index in data){
		var tableRow = document.createElement("tr");

		var dataTitle = document.createElement("td");
		dataTitle.innerHTML = data[index].attributes.QuizName;
		tableRow.appendChild(dataTitle);

		var dataQuestions = document.createElement("td");
		dataQuestions.innerHTML = data[index].attributes.Description;

		tableRow.appendChild(dataQuestions);

		var quizOptions = document.createElement("td");

		var vhref = document.createElement("a");
		vhref.setAttribute("id","" + index);
		vhref.setAttribute("href","#");
		vhref.setAttribute("onclick","viewQuiz(this.id)");
		vhref.innerHTML = "More Details";

		var phref = document.createElement("a");
		phref.setAttribute("id","" + index);
		phref.setAttribute("href","#");
		phref.setAttribute("onclick","printQuiz(this.id)");
		phref.innerHTML = "Print Quiz";

		var dhref = document.createElement("a");
		dhref.setAttribute("id","" + data[index].id);
		dhref.setAttribute("href","#");
		dhref.setAttribute("onclick","deleteQuiz(this.id)");
		dhref.innerHTML = "Delete";

		quizOptions.appendChild(vhref);
		quizOptions.innerHTML = quizOptions.innerHTML + " | ";
		quizOptions.appendChild(phref);
		quizOptions.innerHTML = quizOptions.innerHTML + " | ";
		quizOptions.appendChild(dhref);
		tableRow.appendChild(quizOptions);

		table.appendChild(tableRow);
	}
}

function viewQuiz(index){
	var parentNode = document.getElementById("viewQuiz");
	parentNode.innerHTML = "";

	var quiz = getRelevantQuiz(index);

	var header = document.createElement("h2");
	header.innerHTML = quiz.title;
	parentNode.appendChild(header);

	var desc = document.createElement("p");
	desc.innerHTML = quiz.desc + ". " + quiz.totalMarks + " total marks.";
	parentNode.appendChild(desc);

	var table = document.createElement("table");
	table.setAttribute("style","width:100%");
	table.innerHTML = "<td><b>Title:</b></td><td><b>Details:</b></td><td><b>Answer:</b></td>"

	for(var index in quiz.questions) {
		var tableRow = document.createElement("tr");

		var dataTitle = document.createElement("td");
		dataTitle.innerHTML = quiz.questions[index].attributes.title;
		tableRow.appendChild(dataTitle);

		var dataQuestion = document.createElement("td");
		dataQuestion.innerHTML = quiz.questions[index].attributes.question;
		tableRow.appendChild(dataQuestion);

		var dataAnswer = document.createElement("td");
		dataAnswer.innerHTML = quiz.questions[index].attributes.answer;
		tableRow.appendChild(dataAnswer);

		var marks = document.createElement("td");
		marks.innerHTML = quiz.marks[index];
		tableRow.appendChild(marks);

		table.appendChild(tableRow);
	}

	parentNode.appendChild(table);
	parentNode.innerHTML = parentNode.innerHTML + "<hr>";
}

function deleteQuiz(id){
	var Quiz = Parse.Object.extend("Quiz");
	var quiz = new Parse.Query(Quiz);
	quiz.get(id, {
		success: function(myQuiz){
			myQuiz.destroy({
				success: function(myQuiz){
					location.reload();
				},
				error: function(myQuiz, error){
					console.log(error);
				}
			});
		}
	})

}

function getRelevantQuiz(index){
	var tempQuiz = quizzes[index];
	var quiz = {};
	var quizQsts = [];
	var totalMarks = 0;

	for(var index in tempQuiz.attributes.questionIDs){
		for(var index_2 in questions){
			if(tempQuiz.attributes.questionIDs[index].match(questions[index_2].id)){
				quizQsts.push(questions[index_2]);
			}
		}
	}

	for(var index in tempQuiz.attributes.marks){
		totalMarks += tempQuiz.attributes.marks[index];
	}

	quiz.title = tempQuiz.attributes.QuizName;
	quiz.desc = tempQuiz.attributes.Description;
	quiz.questions = quizQsts;
	quiz.totalMarks = totalMarks;
	quiz.marks = tempQuiz.attributes.marks;

	return quiz;
}

function printQuiz(index){
	var quiz = getRelevantQuiz(index);

	var title = document.getElementsByTagName("title")[0];
	title.innerHTML = quiz.title;

	var parentNode = document.getElementsByTagName("body")[0];
	bodyStorage = parentNode.innerHTML;
	parentNode.innerHTML = "";

	var wrapperDiv = document.createElement("div");

	var header = document.createElement("h1");
	header.innerHTML = quiz.title;
	header.setAttribute("style","text-align: center; font-size: 300%;");


	var desc = document.createElement("p");
	desc.innerHTML = quiz.desc + "<br /><br />" + quiz.totalMarks + " marks";
	desc.setAttribute("style","text-align: center; font-size: 150%");

	wrapperDiv.appendChild(header);
	wrapperDiv.appendChild(desc);
	wrapperDiv.appendChild(document.createElement("hr"));

	for(var index in quiz.questions) {
		wrapperDiv.appendChild(createQuestion(index, quiz.questions[index], quiz.marks[index]));
		wrapperDiv.appendChild(document.createElement("hr"));
	}

	var homepageBtn = document.createElement("button");
	homepageBtn.innerHTML = "Return";
	homepageBtn.setAttribute("onclick","restorePage()");

	wrapperDiv.appendChild(homepageBtn);
	parentNode.appendChild(wrapperDiv);

}

function createQuestion(num, question, marks){
	var questionDiv = document.createElement("div");

	var header = document.createElement("h2");
	header.innerHTML = "Question " + num + ". <br />";

	var desc = document.createElement("p");
	desc.innerHTML = question.attributes.question;


	questionDiv.appendChild(header);
	questionDiv.appendChild(desc);

	if(question.attributes.type.match("qa")){
		var answer = document.createElement("p");
		answer.innerHTML = "Answer: <input style='width:50%;'>";
		questionDiv.appendChild(answer);


	} else if (question.attributes.type.match("tf")){
		var answer = document.createElement("p");
		answer.innerHTML =  " TRUE || FALSE ";
		questionDiv.appendChild(answer);

	} else if (question.attributes.type.match("mc")){
		var mcDiv = document.createElement("div");

		var answers = [question.attributes.answer, question.attributes.fakeOne, question.attributes.fakeTwo, question.attributes.fakeThree];
		var used = [];

		for(var index in answers){
			var ans = Math.floor(Math.random() * answers.length);
			if(used.indexOf(ans) == -1){
				var answer = document.createElement("p");
				answer.innerHTML = "<input type='checkbox'>" + answers[ans];

				mcDiv.appendChild(answer);
			} else {
				used.push(ans);
			}
		}
		questionDiv.innerHTML = questionDiv.innerHTML + "Tick one and only one answer: <br />";
		questionDiv.appendChild(mcDiv);
	}

	var qstValue = document.createElement("h3");
	qstValue.innerHTML = marks + " marks";
	qstValue.setAttribute("style","text-align: right;");

	questionDiv.appendChild(qstValue);
	return questionDiv;
}

function restorePage(){
	var bodyTag = document.getElementsByTagName("body")[0];
	bodyTag.innerHTML = bodyStorage;
}